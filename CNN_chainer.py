# -*- coding: utf-8 -*-
import json
import random
import numpy as np
import chainer
from chainer import cuda
import chainer.functions as F
from chainer import optimizers
import time
from sklearn.datasets import fetch_mldata
from sklearn.cross_validation import train_test_split
import pylab
import matplotlib.pyplot as plt

"""
gpu_flag = 0

if gpu_flag >= 0:
    cuda.check_cuda_available()
xp = cuda.cupy if gpu_flag >= 0 else np

batchsize = 100
n_epoch = 20
"""

#模擬ラベル作成

f = open("song_features.json")
data = json.load(f)
data = np.array(data)

mogi_label = np.zeros(len(data),dtype = np.int)
for i in range(len(data)):
	mogi_label[i] = random.randint(0,4)

print mogi_label

X_train, X_test, y_train, y_test = train_test_split(data, mogi_label, test_size=0.1)

print X_train[0]
X_train = X_train.reshape((len(X_train), 128, 599, 1))
X_test = X_test.reshape((len(X_test), 128, 599, 1))