# -*- coding: utf-8 -*-
import tensorflow as tf
import time
import json
import numpy as np
import librosa
import matplotlib.pyplot as plt
import random

def inference(images_placeholder, keep_prob):

	# 重みを標準偏差0.1の正規分布で初期化
	def weight_variable(shape):
	    initial = tf.truncated_normal(shape, stddev=0.1)
	    return tf.Variable(initial)

	# バイアスを標準偏差0.1の正規分布で初期化
	def bias_variable(shape):
	    initial = tf.constant(0.1, shape=shape)
	    return tf.Variable(initial)

	# 第一層畳み込み層の作成
	def conv2d(x, W):
	    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

	# 2×のプーリング層の作成
	def max_pool_2x128(x):
	    return tf.nn.max_pool(x, ksize=[1, 2, 1, 1],strides=[1, 2, 1, 1], padding='VALID')
	# 4×のプーリング層の作成
	def max_pool_4x128(x):
	    return tf.nn.max_pool(x, ksize=[1, 4, 1, 1],strides=[1, 4, 1, 1], padding='VALID')

	#第一層畳み込み
	with tf.name_scope('conv1') as scope:
		W_conv1 = weight_variable([4, 1, 128, 256])
		b_conv1 = bias_variable([256])

		x_image = tf.reshape(images_placeholder, [-1,599,1,128])

		a = conv2d(x_image, W_conv1)

		if tf.shape(a) == tf.shape(b_conv1):
			print "同じ"
		else :
			print "違う"


		h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)

	#第一層プーリング X4
	with tf.name_scope('pool1') as scope:
		h_pool1 = max_pool_4x128(h_conv1)

	#第二層畳み込み
	with tf.name_scope('conv2') as scope:
		W_conv2 = weight_variable([4, 1, 256, 256])
		b_conv2 = bias_variable([256])
		h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)

	#第二層プーリング X2
	with tf.name_scope('pool2') as scope:
		h_pool2 = max_pool_2x2(h_conv2)

	#フラット化
	with tf.name_scope('fc1') as scope:
		W_fc1 = weight_variable([73 * 1 * 256, 1024])
		b_fc1 = bias_variable([1024])
		h_pool2_flat = tf.reshape(h_pool2, [-1, 73*1*256])
		h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)
		#ドロップ層の設定
		keep_prob = tf.placeholder("float")
		h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

	#第二結合層
	with tf.name_scope('fc2') as scope:
		W_fc2 = weight_variable([1024, NUM_CLASSES])
		b_fc2 = bias_variable([NUM_CLASSES])

	#出力層
	with tf.name_scope('softmax') as scope:
	    y_conv=tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)

	return y_conv

def loss(logits, labels):
    """ lossを計算する関数

    引数:
      logits: ロジットのtensor, float - [batch_size, NUM_CLASSES]
      labels: ラベルのtensor, int32 - [batch_size, NUM_CLASSES]

    返り値:
      cross_entropy: 交差エントロピーのtensor, float

    """

    # 交差エントロピーの計算
    cross_entropy = -tf.reduce_sum(labels*tf.log(logits))
    # TensorBoardで表示するよう指定
    tf.scalar_summary("cross_entropy", cross_entropy)
    return cross_entropy


def training(loss, learning_rate):
    """ 訓練のopを定義する関数

    引数:
      loss: 損失のtensor, loss()の結果
      learning_rate: 学習係数

    返り値:
      train_step: 訓練のop

    """

    train_step = tf.train.AdamOptimizer(learning_rate).minimize(loss)
    return train_step

# 定義用のflagsを作成
flags = tf.app.flags
 
# 値取得用のFLAGSを作成
FLAGS = flags.FLAGS

flags.DEFINE_string('train', 'train.txt', 'File name of train data')
flags.DEFINE_string('test', 'test.txt', 'File name of train data')
flags.DEFINE_string('train_dir', '/tmp/data', 'Directory to put the training data.')
flags.DEFINE_integer('max_steps', 200, 'Number of steps to run trainer.')
flags.DEFINE_integer('batch_size', 10, 'Batch size'
                     'Must divide evenly into the dataset sizes.')
flags.DEFINE_float('learning_rate', 1e-4, 'Initial learning rate.')

NUM_CLASSES = 5
IMAGE_SIZE = 599
IMAGE_PIXELS = IMAGE_SIZE*1*128

if __name__ == '__main__':
    # ファイルを開く
    f = open("song_features.json")
    data = json.load(f)
    data = np.array(data)

    flatten_data = []
    flatten_label = []
    
    
    for line in range(len(data)):
        for_flat = np.array(data[line])
        flatten_data.append(for_flat.flatten().tolist())
        
        #ラベルはとりあえず適当。1-of-K方式。
        tmp = np.zeros(NUM_CLASSES)
        tmp[int(random.randint(0,4))] = 1
        flatten_label.append(tmp)

    #一列に整形した訓練データ
	train_image =   np.asarray(flatten_data)
	train_label = np.asarray(flatten_label)

	test_image = []
    test_label = []

    train_num = 70
    
    for line in range(train_num, len(data)):
        for_flat = np.array(data[line])
        test_image.append(for_flat.flatten().tolist())
        
        #ラベルはとりあえず適当。1-of-K方式。
        tmp = np.zeros(NUM_CLASSES)
        tmp[int(random.randint(0,4))] = 1
        test_label.append(tmp)

	test_image = np.asarray(test_image)
	test_label = np.asarray(test_label)
	
	print "599×128 = "
	print len(train_image[0])
    
	f.close()

	with tf.Graph().as_default():
        # 画像を入れる仮のTensor
		images_placeholder = tf.placeholder("float", shape=(None, IMAGE_PIXELS))
        # ラベルを入れる仮のTensor
        labels_placeholder = tf.placeholder("float", shape=(None, NUM_CLASSES))
        # dropout率を入れる仮のTensor
        keep_prob = tf.placeholder("float")

        # inference()を呼び出してモデルを作る
        logits = inference(images_placeholder, keep_prob)
        # loss()を呼び出して損失を計算
        loss_value = loss(logits, labels_placeholder)
        # training()を呼び出して訓練
        train_op = training(loss_value, FLAGS.learning_rate)
        # 精度の計算
        acc = accuracy(logits, labels_placeholder)

        # 保存の準備
        saver = tf.train.Saver()
        # Sessionの作成
        sess = tf.Session()
        # 変数の初期化
        sess.run(tf.initialize_all_variables())
        # TensorBoardで表示する値の設定
        summary_op = tf.merge_all_summaries()
        summary_writer = tf.train.SummaryWriter(FLAGS.train_dir, sess.graph_def)
        
        # 訓練の実行
        for step in range(FLAGS.max_steps):
            for i in range(len(train_image)/FLAGS.batch_size):
                # batch_size分の画像に対して訓練の実行
                batch = FLAGS.batch_size*i
                # feed_dictでplaceholderに入れるデータを指定する
                sess.run(train_op, feed_dict={
                  images_placeholder: train_image[batch:batch+FLAGS.batch_size],
                  labels_placeholder: train_label[batch:batch+FLAGS.batch_size],
                  keep_prob: 0.5})

            # 1 step終わるたびに精度を計算する
            train_accuracy = sess.run(acc, feed_dict={
                images_placeholder: train_image,
                labels_placeholder: train_label,
                keep_prob: 1.0})
            print "step %d, training accuracy %g"%(step, train_accuracy)

            # 1 step終わるたびにTensorBoardに表示する値を追加する
            summary_str = sess.run(summary_op, feed_dict={
                images_placeholder: train_image,
                labels_placeholder: train_label,
                keep_prob: 1.0})
            summary_writer.add_summary(summary_str, step)

    # 訓練が終了したらテストデータに対する精度を表示
    print "test accuracy %g"%sess.run(acc, feed_dict={
        images_placeholder: test_image,
        labels_placeholder: test_label,
        keep_prob: 1.0})

    # 最終的なモデルを保存
    save_path = saver.save(sess, "model.ckpt")
