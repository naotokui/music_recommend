# -*- coding: utf-8 -*-
import json
import numpy as np
import random

if __name__ == '__main__':
	f = open("song_features.json")
	data = json.load(f)
	data = np.array(data)

	flatten_data = []
	flatten_label = []
    
    
	for line in range(len(data)):
		for_flat = np.array(data[line])
		flatten_data.append(for_flat.flatten().tolist())
        
        #ラベルはとりあえず適当。1-of-K方式。
		tmp = np.zeros(5)
		tmp[int(random.randint(0,4))] = 1
		flatten_label.append(tmp)

    #一列に整形した訓練データ

	for i in range(len(flatten_data)):
		print len(flatten_data[i])